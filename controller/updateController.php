<?php

include_once __DIR__."/../model/Article.php";

$timestamp = date('Y-m-d H:i:s', time());

if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {
    $article = new Article();
    $article->update($_POST['id'], $_POST['name'], $_POST['description'], $_POST['created_at']);

    header('Location: ../index.php');
}

    $id = $_GET['id'];
    $art = new Article();
    $rows = $art->findById($id);



    require_once __DIR__.'/../view/updateView.php';
