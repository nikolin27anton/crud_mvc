<?php

class Article
{
    private function getConnection()
    {
        //$pdoConnection = new PDO("postgres", "postgres", "0000");
        $pdoConnection = new PDO('pgsql:host=127.0.0.1;dbname=postgres', 'postgres', '0000');

        return $pdoConnection;
    }

    public function insert($name, $description, $created_at)
    {
        $pdoConnection = $this->getConnection();

        $sql = 'INSERT INTO article (name, description , created_at) VALUES (:name, :description, :created_at)';

        $statement = $pdoConnection->prepare($sql);

        $statement->bindValue(':name', $name);
        $statement->bindValue(':description', $description);
        $statement->bindValue(':created_at', $created_at);

        $statement->execute();

        $statement->errorInfo();
    }

    public function findAll()
    {
        $pdoConnection = $this->getConnection();

        $sql = 'SELECT * FROM article';

        $statement = $pdoConnection->prepare($sql);

        $statement->execute();


        return $statement->fetchAll(); //Возвращает массив, содержащий все строки набора результатов
    }

    public function update($id, $name, $description, $created_at)
    {
        $pdoConnection = $this->getConnection();

        $sql = 'UPDATE article SET name = :name, description =:description, created_at = :created_at WHERE id = :id';

        $statement = $pdoConnection->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->bindValue(':name', $name);
        $statement->bindValue(':description', $description);
        $statement->bindValue(':created_at', $created_at);

        return $statement->execute();
    }

    public function findById($id)
    {
        $pdoConnection = $this->getConnection();

        $sql = 'SELECT * FROM article WHERE id = :id';
        $statement = $pdoConnection->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();

        return $statement->fetch();//Извлекает следующую строку из результирующего набора объекта PDOStatement
    }

    public function deleteById($id)
    {
        $pdoConnection = $this->getConnection();

        $sql = 'DELETE  FROM  article WHERE id = :id';
        $statement = $pdoConnection->prepare($sql);
        $statement->bindValue(':id', $id);

        return $statement->execute();
    }
}
